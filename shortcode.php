<?php
/**
 * Class Vertical_Posts_Slider_Shortcode
 * Shortcode for display posts in with slider
 *
 * @since 1.0.0
 */

defined( 'ABSPATH' ) || exit;

class Vertical_Posts_Slider_Shortcode {

	/**
	 * Vertical_Posts_Slider_Shortcode initialization class.
	 *
	 * @since 1.0.0
	 */
	public function init() {

		add_shortcode( 'vps-posts', array( $this, 'add_slider' ) );
		add_action( 'wp_enqueue_scripts', array( $this, 'add_scripts' ), 10 );
        add_shortcode( 'vps-posts', array( $this, 'add_slider' ) );

	}

	/**
	 * Add scripts and styles for frontend
	 *
	 * @since 1.0.1
	 */
	public function add_scripts() {

		wp_enqueue_style( 'vertical-posts-slider-styles', VPS_PLUGIN_URL . '/assets/style.css', array(), time() );

		wp_enqueue_script( 'vertical-posts-slider-scripts', VPS_PLUGIN_URL . '/assets/scripts.js', array(
			'jquery',
		), time(), true );

	}

    /**
     * Posts shortcode
     *
     * @since 1.0.0
     *
     * @param $atts
     * @return string
     */
	public function add_slider( $atts ) {
		global $post;

        $arg = (object) shortcode_atts( [
            'items' => 10
        ], $atts );

		ob_start();

		$posts = get_posts( array(
			'posts_per_page' => $arg->items,
			'order'          => 'ASC',
			'orderby'        => 'title'
		) );

		?>
        <div class="vps-frame">
            <div class="vps-wrapper">
				<?php if ( count( $posts ) ) {
					$postCounter = 0;
					foreach ( $posts as $post ) {
						setup_postdata( $post );

						if ( ! has_post_thumbnail() ) {
							continue;
						}

						$postCounter ++;
						$class = 1 == $postCounter || 3 == $postCounter ? 'inactive' : 'hided';
						$class = 2 == $postCounter ? 'active' : $class;
						?>
                        <div class="vps-wrapper-post <?php esc_attr_e( $class ); ?>">
							<?php if ( 3 < count( $posts ) ) { ?>
                                <span class="vps-wrapper-post__before"></span>
							<?php } ?>

                            <div class="vps-wrapper-post__image">
								<?php the_post_thumbnail(); ?>
                            </div>
                            <div class="vps-wrapper-post__description">
								<?php echo the_title(); ?>
                            </div>
                            <div class="vps-wrapper-post__info">
                                <a href="<?php echo get_permalink(); ?>">
									<?php _e( 'Read more >' ); ?>
                                </a>
                                <span class="vps-wrapper-post__date">
                                    <?php the_date( 'j F, Y' ); ?>
                                </span>
                            </div>

							<?php if ( 3 < count( $posts ) ) { ?>
                                <span class="vps-wrapper-post__after"></span>
							<?php } ?>

                        </div>
					<?php } ?>
					<?php wp_reset_postdata(); ?>
				<?php } ?>

            </div>
        </div>
		<?php

		return ob_get_clean();
	}

}