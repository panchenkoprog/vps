<?php

/**
 * Init class for Vertical Posts Slider plugin
 *
 * @since 1.0.0
 */

defined( 'ABSPATH' ) || exit;

class Vertical_Posts_Slider {

	/**
	 * The one and only true Vertical_Posts_Slider instance
	 *
	 * @since 1.0.0
	 * @access private
	 * @var object $instance
	 */
	private static $instance;

	/**
	 * Plugin version
	 *
	 * @since 1.0.0
	 * @var string
	 */
	private $version = '1.0.0';

	/**
	 * Instantiate the main class
	 *
	 * This function instantiates the class, initialize all functions and return the object.
	 *
	 * @since 1.0.0
	 * @return object The one and only true Vertical_Posts_Slider instance.
	 */
	public static function instance() {

		if ( ! isset( self::$instance ) && ( ! self::$instance instanceof Vertical_Posts_Slider ) ) {

			self::$instance = new Vertical_Posts_Slider;
			self::$instance->set_up_constants();
			self::$instance->includes();

		}

		return self::$instance;
	}

    /**
     * Constructor
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function __construct() {

        add_action( 'plugins_loaded', [ $this, 'init' ] );

    }

    /**
     * Initialize the plugin
     * Fired by `plugins_loaded` action hook.
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init() {

        // Add Plugin actions
        add_action( 'elementor/widgets/widgets_registered', [ $this, 'init_widgets' ] );
        add_action( 'elementor/controls/controls_registered', [ $this, 'init_controls' ] );

    }

	/**
	 * Function for setting up constants
	 *
	 * This function is used to set up constants used throughout the plugin.
	 *
	 * @since 1.0.0
	 */
	public function set_up_constants() {

		self::set_up_constant( 'VPS_VERSION', $this->version );
		self::set_up_constant( 'VPS_PLUGIN_PATH', plugin_dir_path( __FILE__ ) . '../' );
		self::set_up_constant( 'VPS_PLUGIN_URL', plugin_dir_url( __FILE__ ) . '../' );
		self::set_up_constant( 'VPS_LIBRARIES_PATH', plugin_dir_path( __FILE__ ) );

	}

	/**
	 * Make new constants
	 *
	 * @param string $name
	 * @param mixed $val
	 *
	 * @since 1.0.0
	 */
	public static function set_up_constant( $name, $val = false ) {

		if ( ! defined( $name ) ) {
			define( $name, $val );
		}

	}

	/**
	 * Includes all necessary PHP files
	 *
	 * This function is responsible for including all necessary PHP files.
	 *
	 * @since 1.0.0
	 */
	public function includes() {

		if ( defined( 'VPS_LIBRARIES_PATH' ) ) {
			require VPS_LIBRARIES_PATH . '/shortcode.php';
        }

	}

    /**
     * Includes all necessary PHP files
     *
     * This function is responsible for including all necessary Elementor Widget files.
     *
     * @since 1.0.0
     */
    public function init_widgets() {

        if ( defined( 'VPS_LIBRARIES_PATH' ) ) {
            // Include Widget files
            require VPS_LIBRARIES_PATH . '/vps-elementor-widget.php';

            // Register widget
            \Elementor\Plugin::instance()->widgets_manager->register_widget_type( new \VPS_Elementor_Widget() );
        }

    }


    /**
     * Init Controls
     *
     * Include controls files and register them
     *
     * @since 1.0.0
     *
     * @access public
     */
    public function init_controls() {

        // Include Control files

    }

}