<?php

/**
 * Class VPS_Elementor_Widget
 *
 * VPS Elementor Widget.
 *
 * Elementor widget that inserts Vertical Posts Slider into the page.
 *
 * @since 1.0.0
 */
class VPS_Elementor_Widget extends \Elementor\Widget_Base {

    /**
     * Get widget name.
     *
     * Retrieve widget name.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget name.
     */
    public function get_name() {
        return 'Vertical Posts Slider';
    }

    /**
     * Get widget title.
     *
     * Retrieve widget title.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget title.
     */
    public function get_title() {
        return __( 'Vertical Posts Slider', 'plugin-name' );
    }

    /**
     * Get widget icon.
     *
     * Retrieve widget icon.
     *
     * @since 1.0.0
     * @access public
     *
     * @return string Widget icon.
     */
    public function get_icon() {
        return 'eicon-image-box';
    }

    /**
     * Get widget categories.
     *
     * Retrieve the list of categories the widget belongs to.
     *
     * @since 1.0.0
     * @access public
     *
     * @return array Widget categories.
     */
    public function get_categories() {
        return [ 'general' ];
    }

    /**
     * Register widget controls.
     *
     * Adds different input fields to allow the user to change and customize the widget settings.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function _register_controls() {

        $this->start_controls_section(
            'content_section',
            [
                'label' => __( 'Content', 'plugin-vps' ),
                'tab' => \Elementor\Controls_Manager::TAB_CONTENT,
            ]
        );

        $this->add_control(
            'number_of_posts',
            [
                'label' => __( 'Number of posts', 'plugin-vps' ),
                'type' => \Elementor\Controls_Manager::NUMBER,
                'input_type' => 'number',
                'placeholder' => __( '10', 'plugin-vps' ),
                'min' => 1,
                'max' => 10
            ]
        );

        $this->end_controls_section();

    }

    /**
     * Render widget output on the frontend.
     *
     * Written in PHP and used to generate the final HTML.
     *
     * @since 1.0.0
     * @access protected
     */
    protected function render() {

        $settings = $this->get_settings_for_display();
        $number_of_posts = $settings['number_of_posts'] ? $settings['number_of_posts'] : 10;

        echo '<div class="vps-elementor-widget">';

        echo do_shortcode('[vps-posts'.($number_of_posts ? " items=\"$number_of_posts\"" : '').']');

        echo '</div>';

    }

}